package com.bay.register.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

import com.bay.register.db.entity.UserInfo;
import com.bay.register.db.repository.UserInfoRepository;
import com.bay.register.model.RegisteredUser;
import com.bay.register.model.UserProfile;
import com.bay.register.service.ErrorCode;
import com.bay.register.service.Gender;
import com.bay.register.service.ServiceResult;
import com.bay.register.service.UserInfoServiceImpl;

@ActiveProfiles("test")
@RunWith(MockitoJUnitRunner.class)
public class UserInfoServiceTest {
	
	@Mock
	UserProfile userProfileMock;
	
	@Mock
	UserInfo userInfoMock;
	
	@Mock
	UserInfoRepository userInfoRepositoryMock;
	
	@InjectMocks
	UserInfoServiceImpl userInfoService;
	
	@Before
	public void initial() {
		when(userProfileMock.getUsername()).thenReturn("username");
		when(userProfileMock.getPassword()).thenReturn("password");
		when(userProfileMock.getFirstName()).thenReturn("firstname");
		when(userProfileMock.getLastName()).thenReturn("lastname");
		when(userProfileMock.getGender()).thenReturn(Gender.MALE);
		when(userProfileMock.getAddress()).thenReturn("address");
		when(userProfileMock.getMobile()).thenReturn("0815433112");
		when(userProfileMock.getSalary()).thenReturn(16000l);
		
		when(userInfoRepositoryMock.save(Mockito.any(UserInfo.class))).thenReturn(userInfoMock);
	}
	
	@Test
	public void testValidateUserProfileUserName() {
		when(userProfileMock.getUsername()).thenReturn(null);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_VALIDATE_BLANK, serviceResult.getErrorCode());
		assertEquals("Username is required", serviceResult.getErrorMessage());
	}
	
	@Test
	public void testValidateUserProfilePassword() {
		when(userProfileMock.getPassword()).thenReturn(null);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_VALIDATE_BLANK, serviceResult.getErrorCode());
		assertEquals("Password is required", serviceResult.getErrorMessage());
	}
	
	@Test
	public void testValidateUserProfileFirstName() {
		when(userProfileMock.getFirstName()).thenReturn(null);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_VALIDATE_BLANK, serviceResult.getErrorCode());
		assertEquals("First name is required", serviceResult.getErrorMessage());
		
		Mockito.reset(userProfileMock);
	}
	
	@Test
	public void testValidateUserProfileLastName() {
		when(userProfileMock.getLastName()).thenReturn(null);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_VALIDATE_BLANK, serviceResult.getErrorCode());
		assertEquals("Last name is required", serviceResult.getErrorMessage());
	}
	
	@Test
	public void testValidateUserProfileGender() {
		when(userProfileMock.getGender()).thenReturn(null);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertTrue(serviceResult.isSuccess());
	}
	
	@Test
	public void testValidateUserProfileAddress() {
		when(userProfileMock.getAddress()).thenReturn(null);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_VALIDATE_BLANK, serviceResult.getErrorCode());
		assertEquals("Address is required", serviceResult.getErrorMessage());
	}
	
	@Test
	public void testValidateUserProfileMobile() {
		when(userProfileMock.getMobile()).thenReturn(null);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_VALIDATE_BLANK, serviceResult.getErrorCode());
		assertEquals("Mobile is required", serviceResult.getErrorMessage());
	}
	
	@Test
	public void testValidateUserProfileMobileFormat() {
		when(userProfileMock.getMobile()).thenReturn("123456789");
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_VALIDATE_FORMAT, serviceResult.getErrorCode());
		assertEquals("Mobile number is invalid", serviceResult.getErrorMessage());
	}
	
	@Test
	public void testValidateUserProfileSalary() {
		when(userProfileMock.getSalary()).thenReturn(0l);
		ServiceResult<RegisteredUser> serviceResult = userInfoService.register(userProfileMock);
		
		assertFalse(serviceResult.isSuccess());
		assertEquals(ErrorCode.REGISTER_REJECT, serviceResult.getErrorCode());
		assertEquals("Salary is less than 15000 baht", serviceResult.getErrorMessage());
	}
	
	@Test
	public void testGetUserType() {
		assertNull(userInfoService.getUserType(0));
		assertEquals(new Character('S'), userInfoService.getUserType(15000));
		assertEquals(new Character('S'), userInfoService.getUserType(15001));
		assertEquals(new Character('S'), userInfoService.getUserType(29999));
		assertEquals(new Character('G'), userInfoService.getUserType(30000));
		assertEquals(new Character('G'), userInfoService.getUserType(50000));
		assertEquals(new Character('P'), userInfoService.getUserType(50001));
	}
	
}
