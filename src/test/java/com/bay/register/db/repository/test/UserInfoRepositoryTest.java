package com.bay.register.db.repository.test;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bay.register.db.entity.UserInfo;
import com.bay.register.db.repository.UserInfoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest
public class UserInfoRepositoryTest {

	@Autowired
	UserInfoRepository userInforepository;

	@Test
	public void save() throws Exception {
		UserInfo userInfo = new UserInfo();
		userInfo.setUsername("username");
		userInfo.setPassword("password");
		userInfo.setAddress("addr1");
		userInfo.setFirstName("firstname");
		userInfo.setLastName("lastName");
		userInfo.setMobile("0815433112");
		userInfo.setGender('M');
		userInfo.setRefCode("1234");
		userInfo.setSalary(75000);
		userInfo.setUserType('P');
		userInfo.setCreatedDttm(new Date());

		this.userInforepository.save(userInfo);
		
		UserInfo registedUser = this.userInforepository.findByUsername("username");
		Assert.assertNotNull(registedUser);
	}
	
	@Test
	public void saveUserNameDup() throws Exception {
		UserInfo userInfo = new UserInfo();
		userInfo.setUsername("username");
		userInfo.setPassword("password");
		userInfo.setAddress("addr1");
		userInfo.setFirstName("firstname");
		userInfo.setLastName("lastName");
		userInfo.setMobile("0815433112");
		userInfo.setGender('M');
		userInfo.setRefCode("1234");
		userInfo.setSalary(75000);
		userInfo.setUserType('P');
		userInfo.setCreatedDttm(new Date());

		this.userInforepository.save(userInfo);
		this.userInforepository.save(userInfo);
		
		UserInfo registedUser = this.userInforepository.findByUsername("username");
		Assert.assertNotNull(registedUser);
	}
}
