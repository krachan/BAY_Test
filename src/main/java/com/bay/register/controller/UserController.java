package com.bay.register.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bay.register.model.RegisterResponse;
import com.bay.register.model.RegisteredUser;
import com.bay.register.model.UserProfile;
import com.bay.register.service.ServiceResult;
import com.bay.register.service.TokenProvider;
import com.bay.register.service.UserInfoService;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserInfoService userInfoService;
	
	@Autowired
	private TokenProvider tokenProvider;

	@RequestMapping(value = "/user/register", method = RequestMethod.POST)
	public ResponseEntity<?> register(@RequestBody UserProfile userProfile, HttpServletRequest request) {
		RegisterResponse response = new RegisterResponse();		
		String authToken = tokenProvider.getJwtFromRequest(request);
		System.out.println("authToken = " + authToken);
		
		ServiceResult<String> validateTokenResult = tokenProvider.validateToken(authToken);
		if(!validateTokenResult.isSuccess()) {
			response.setSuccess(false);
			response.setErrorCode(validateTokenResult.getErrorCode().getCode());
			response.setErrorMessage(validateTokenResult.getErrorMessage());

			return new ResponseEntity<RegisterResponse>(response, HttpStatus.OK);
		}
		
		ServiceResult<RegisteredUser> result = userInfoService.register(userProfile);
		if (result.isSuccess()) {
			response.setErrorCode(result.getErrorCode().getCode());
			response.setErrorMessage(result.getErrorMessage());
			response.setSuccess(true);
			response.setRegisteredUser(result.getObjResult());

			return new ResponseEntity<RegisterResponse>(response, HttpStatus.OK);
		} else {
			response.setSuccess(false);
			response.setErrorCode(result.getErrorCode().getCode());
			response.setErrorMessage(result.getErrorMessage());

			return new ResponseEntity<RegisterResponse>(response, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable("username") String username, HttpServletRequest request) {
		RegisterResponse response = new RegisterResponse();
		
		String authToken = tokenProvider.getJwtFromRequest(request);
		System.out.println("authToken = " + authToken);
		
		ServiceResult<String> validateTokenResult = tokenProvider.validateToken(authToken);
		if(!validateTokenResult.isSuccess()) {
			response.setSuccess(false);
			response.setErrorCode(validateTokenResult.getErrorCode().getCode());
			response.setErrorMessage(validateTokenResult.getErrorMessage());

			return new ResponseEntity<RegisterResponse>(response, HttpStatus.OK);
		}
		
		ServiceResult<RegisteredUser> result = userInfoService.getUserInfoByUsername(username);
		
		if (result.isSuccess()) {
			response.setErrorCode(result.getErrorCode().getCode());
			response.setErrorMessage(result.getErrorMessage());
			response.setSuccess(true);
			response.setRegisteredUser(result.getObjResult());

			return new ResponseEntity<RegisterResponse>(response, HttpStatus.OK);
		} else {
			response.setSuccess(false);
			response.setErrorCode(result.getErrorCode().getCode());
			response.setErrorMessage(result.getErrorMessage());

			return new ResponseEntity<RegisterResponse>(response, HttpStatus.OK);
		}
	}
}
