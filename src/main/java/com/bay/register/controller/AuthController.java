package com.bay.register.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bay.register.model.AuthenticationRequest;
import com.bay.register.model.JwtAuthenticationResponse;
import com.bay.register.service.TokenProvider;

@RestController
@RequestMapping("/api")
public class AuthController {
	private static String AUTH_USERNAME = "BAYAPP453";
	private static String AUTH_PASSWORD = "P@ssW0rd";

	@Autowired
	private TokenProvider tokenProvider;
	
//	@PostMapping("/authen")
	@RequestMapping(value = "/authen", method = RequestMethod.POST)
	public ResponseEntity<?> authenticateUser(@RequestBody AuthenticationRequest authenticationRequest) {

		// Authentication authentication = authenticationManager.authenticate(
		// new UsernamePasswordAuthenticationToken(
		// loginRequest.getUsernameOrEmail(),
		// loginRequest.getPassword()
		// )
		// );
		// SecurityContextHolder.getContext().setAuthentication(authentication);

		if (AUTH_USERNAME.equals(authenticationRequest.getUsername())
				&& AUTH_PASSWORD.equals(authenticationRequest.getPassword())) {
			String jwt = tokenProvider.createToken();
			return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}
}
