package com.bay.register.db.repository;

import org.springframework.data.repository.CrudRepository;

import com.bay.register.db.entity.UserInfo;

public interface UserInfoRepository extends CrudRepository<UserInfo, Long>{
	public UserInfo findByUsername(String username);
}
