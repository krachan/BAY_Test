package com.bay.register.db.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserInfo {
	public static final char MALE = 'M';
	public static final char FEMALE = 'F';
	public static final char PLATINUM = 'P';
	public static final char GOLD = 'G';
	public static final char SILVER = 'S';
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String refCode;
	
	@Column(unique = true)
	private String username;
	
	private String password;
	private String firstName;
	private String lastName;
	/*
	 * M:Male F:Female
	 */
	private char gender;
	private String address;
	private String mobile;
	private long salary;
	/*
	 * P:Platinum G:Gold S:Silver
	 */
	private char userType;
	private Date createdDttm;

	public long getId() {
		return this.id;
	}
	
	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public char getUserType() {
		return userType;
	}

	public void setUserType(char userType) {
		this.userType = userType;
	}

	public Date getCreatedDttm() {
		return createdDttm;
	}

	public void setCreatedDttm(Date createdDttm) {
		this.createdDttm = createdDttm;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

}
