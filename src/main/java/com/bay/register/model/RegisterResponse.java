package com.bay.register.model;

public class RegisterResponse {
	private boolean success;
	private String errorCode;
	private String errorMessage;
	private RegisteredUser registeredUser;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public RegisteredUser getRegisteredUser() {
		return registeredUser;
	}

	public void setRegisteredUser(RegisteredUser registeredUser) {
		this.registeredUser = registeredUser;
	}

	
//	public String getReferenceCode() {
//		return referenceCode;
//	}
//
//	public void setReferenceCode(String referenceCode) {
//		this.referenceCode = referenceCode;
//	}

}
