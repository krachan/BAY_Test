package com.bay.register.service;

public class ServiceResult<T> {
	private boolean success;
	private ErrorCode errorCode;
	private String errorMessage;
	
	private T objResult;
	
	public ServiceResult() {
		this.success = true;
		this.errorCode = ErrorCode.SUCCESS;
	}
	
	public ServiceResult(ErrorCode errorCode) {
		this.success = false;
		this.errorCode = errorCode;
		this.errorMessage = errorCode.getDescription();
	}
	
	public ServiceResult(ErrorCode errorCode, String errorMessage) {
		this.success = false;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public T getObjResult() {
		return objResult;
	}

	public void setObjResult(T objResult) {
		this.objResult = objResult;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
