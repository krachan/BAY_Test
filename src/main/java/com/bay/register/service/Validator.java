package com.bay.register.service;

public class Validator {
	public static boolean isEmpty(Object obj) {
		if(obj == null) {
			return true;
		}
		
		if(obj instanceof String) {
			String str = (String) obj;
			str = str.trim();
			return str.isEmpty();
		}
		
		return false;
	}
	
	private static String mobileRegEx = "^0[0-9]{9}$";
	public static boolean isMobile(String mobile) {
		if(!isEmpty(mobile)) {
			return mobile.matches(mobileRegEx);
		}
		
		return false;
	}
	
	public static void main(String args[]) {
		System.out.println(Validator.isMobile("1"));
		System.out.println(Validator.isMobile("1234567890"));
		System.out.println(Validator.isMobile("0815433112"));
	}
}
