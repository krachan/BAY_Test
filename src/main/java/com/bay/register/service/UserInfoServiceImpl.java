package com.bay.register.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bay.register.db.entity.UserInfo;
import com.bay.register.db.repository.UserInfoRepository;
import com.bay.register.model.RegisteredUser;
import com.bay.register.model.UserProfile;

@Service
public class UserInfoServiceImpl implements UserInfoService {
	
	@Autowired
	private UserInfoRepository userInforRepository;
	
	@Override
	public ServiceResult<RegisteredUser> register(UserProfile userProfile) {		
		
		ServiceResult<RegisteredUser> validatedResult = validate(userProfile);
		if(!validatedResult.isSuccess()) {
			return validatedResult;
		}
		
		Character userType = getUserType(userProfile.getSalary());
		if(userType == null) {
			return new ServiceResult<>(ErrorCode.REGISTER_REJECT);
		}
		
		//check duplicate user name
		UserInfo duplicatedUser = userInforRepository.findByUsername(userProfile.getUsername());
		if(duplicatedUser != null) {
			return new ServiceResult<>(ErrorCode.REGISTER_DUPLICATED);
		}
		
		UserInfo userInfo = new UserInfo();
		userInfo.setUsername(userProfile.getUsername());
		userInfo.setPassword(userProfile.getPassword());
		userInfo.setFirstName(userProfile.getFirstName());
		userInfo.setLastName(userProfile.getLastName());
		
		if(userProfile.getGender() == Gender.FEMALE) {
			userInfo.setGender(UserInfo.FEMALE);		
		} else if(userProfile.getGender() == Gender.MALE) {
			userInfo.setGender(UserInfo.MALE);			
		}
		
		userInfo.setAddress(userProfile.getAddress());
		userInfo.setMobile(userProfile.getMobile());
		userInfo.setSalary(userProfile.getSalary());
		
		Date createdDate = new Date();
		
		userInfo.setRefCode(getRefCode(createdDate, userProfile.getMobile()));
		userInfo.setUserType(getUserType(userProfile.getSalary()));
		userInfo.setCreatedDttm(createdDate);
		
		UserInfo savedUserInfo = userInforRepository.save(userInfo);
		
		RegisteredUser registeredUser = getRegisteredUser(savedUserInfo);
		ServiceResult<RegisteredUser> serviceResult = new ServiceResult<RegisteredUser>();
		serviceResult.setObjResult(registeredUser);
		
		return serviceResult;
	}
	
	public String getRefCode(Date createdDate, String mobile) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String refCode = sdf.format(createdDate) + mobile.substring(mobile.length()-4, mobile.length());
		return refCode;
	}
	
	public Character getUserType(long salary) {
		if(salary < 15000) {
			return null;
		} else if(salary < 30000) {
			return UserInfo.SILVER;
		} else if(salary <= 50000) {
			return UserInfo.GOLD;
		} else {
			return UserInfo.PLATINUM;
		}
	}
	
	public String getUserType(char userType) {
		if(UserInfo.PLATINUM == userType) {
			return "Platinum";
		} else if(UserInfo.GOLD == userType) {
			return "Gold";
		} else if(UserInfo.SILVER == userType) {
			return "Silver";
		} else {
			return null;
		}
	}
	
	private ServiceResult<RegisteredUser> validate(UserProfile userProfile) {
		if(Validator.isEmpty(userProfile.getUsername())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_BLANK, "Username is required");
		}
		
		if(Validator.isEmpty(userProfile.getPassword())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_BLANK, "Password is required");
		}
		
		if(Validator.isEmpty(userProfile.getFirstName())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_BLANK, "First name is required");
		}
		
		if(Validator.isEmpty(userProfile.getLastName())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_BLANK, "Last name is required");
		}
		
//		if(Validator.isEmpty(userProfile.getGender())) {
//			return new ServiceResult<UserProfile>(ErrorCode.REGISER_VALIDATE_BLANK, "First name is required");
//		}
		
		if(Validator.isEmpty(userProfile.getAddress())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_BLANK, "Address is required");
		}
		
		if(Validator.isEmpty(userProfile.getMobile())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_BLANK, "Mobile is required");
		}
		
		if(!Validator.isMobile(userProfile.getMobile())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_FORMAT, "Mobile number is invalid");
		}
		
		if(Validator.isEmpty(userProfile.getSalary())) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTER_VALIDATE_BLANK, "Salary is required");
		}
		
		return new ServiceResult<RegisteredUser>();
	}

	@Override
	public ServiceResult<RegisteredUser> getUserInfoByUsername(String username) {
		if(username == null) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTERED_USER_NULL);
		}
		
		UserInfo userInfo = userInforRepository.findByUsername(username);
		if(userInfo == null) {
			return new ServiceResult<RegisteredUser>(ErrorCode.REGISTERED_USER_NULL);
		}
		
		RegisteredUser registeredUser = getRegisteredUser(userInfo);
		ServiceResult<RegisteredUser> serviceResult = new ServiceResult<RegisteredUser>();
		serviceResult.setObjResult(registeredUser);
		
		return serviceResult;
	}
	
	public RegisteredUser getRegisteredUser(UserInfo userInfo) {
		RegisteredUser registeredUser = new RegisteredUser();
		registeredUser.setAddress(userInfo.getAddress());
		registeredUser.setFirstName(userInfo.getFirstName());
		
		if(userInfo.getGender() == UserInfo.MALE) {
			registeredUser.setGender(Gender.MALE.name());
		}
		
		if(userInfo.getGender() == UserInfo.FEMALE) {
			registeredUser.setGender(Gender.FEMALE.name());
		}
		
		registeredUser.setLastName(userInfo.getLastName());
		registeredUser.setMobile(userInfo.getMobile());
		registeredUser.setRefCode(userInfo.getRefCode());
		registeredUser.setSalary(userInfo.getSalary());
		registeredUser.setUsername(userInfo.getUsername());
		registeredUser.setUserType(getUserType(userInfo.getUserType()));
		
		return registeredUser;
	}
}
