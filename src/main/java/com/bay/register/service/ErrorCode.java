package com.bay.register.service;

public enum ErrorCode {
	
	SUCCESS("0000","Success"),
	REGISTER_REJECT("3001", "Salary is less than 15000 baht"),
	REGISTER_VALIDATE_BLANK("3002", "Parameter required"),
	REGISTER_VALIDATE_FORMAT("3003", "Parameter format is invalid"),
	REGISTER_DUPLICATED("3004", "User name already existed"),
	REGISTERED_USER_NULL("3005", "User not found"),
	TOKEN_INVALID("4001", "Token invalid");
	
	private String code;
	private String description;
	
	ErrorCode(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public String getDescription() {
		return this.description;
	}
	
}
