package com.bay.register.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Service
public class TokenProvider {
	private static String KEY = "B1A2Y3K4E5Y6";

	public String createToken() {

		String compactJws = Jwts.builder().setSubject("BAY").signWith(SignatureAlgorithm.HS512, KEY).compact();

		return compactJws;
	}

	public ServiceResult<String> validateToken(String authToken) {
		if(authToken == null) {
			return new ServiceResult<>(ErrorCode.TOKEN_INVALID);
		}
		
		String errorMessage = null;
		try {
			Jwts.parser().setSigningKey(KEY).parseClaimsJws(authToken);
			return new ServiceResult<String>();
		} catch (SignatureException ex) {
			errorMessage = "Invalid JWT signature";
		} catch (MalformedJwtException ex) {
			errorMessage = "Invalid JWT token";
		} catch (ExpiredJwtException ex) {
			errorMessage = "Expired JWT token";
		} catch (UnsupportedJwtException ex) {
			errorMessage = "Unsupported JWT token";
		} catch (IllegalArgumentException ex) {
			errorMessage = "JWT claims string is empty.";
		}

		return new ServiceResult<>(ErrorCode.TOKEN_INVALID, errorMessage);
	}

	public String getJwtFromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		
		return null;
	}
}
