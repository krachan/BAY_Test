package com.bay.register.service;

import com.bay.register.model.RegisteredUser;
import com.bay.register.model.UserProfile;

public interface UserInfoService {
	public ServiceResult<RegisteredUser> register(UserProfile userProfile);
	public ServiceResult<RegisteredUser> getUserInfoByUsername(String username);
}
